import 'package:app_drug_daily_dose/model/drug_dose_list.dart';

class DrugDoseResponse {
  DrugDoseList? body;

  DrugDoseResponse({this.body});

  factory DrugDoseResponse.fromJson(Map<String, dynamic> json) {
    return DrugDoseResponse(
        body: json['body'] != null ? DrugDoseList.fromJson(json['body']) : null,
    );
  }
}