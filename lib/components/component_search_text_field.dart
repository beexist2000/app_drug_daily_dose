import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class ComponentSearchTextField extends StatefulWidget {
  const ComponentSearchTextField({super.key, required this.name, required this.initialValue, required this.labelText});

  final String name;
  final String initialValue;
  final String labelText;

  @override
  State<ComponentSearchTextField> createState() => _ComponentSearchTextFieldState();
}

class _ComponentSearchTextFieldState extends State<ComponentSearchTextField> {
  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      name: widget.name,
      initialValue: widget.initialValue,
      decoration: InputDecoration(
        labelText: widget.labelText,
        enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey)
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
      ),
    );
  }
}

