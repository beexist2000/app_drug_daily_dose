import 'package:app_drug_daily_dose/model/drug_dose_list_item.dart';

class DrugDoseList {
  int currentPage;
  int totalPage;
  int totalItemCount;
  List<DrugDoseListItem> items;

  DrugDoseList(this.currentPage, this.totalPage, this.totalItemCount, this.items);

  factory DrugDoseList.fromJson(Map<String, dynamic> json) {
    int totalItemCount = json['totalCount'] != null ? (json['totalCount'] as int) : 0;
    int numOfRows = json['numOfRows'] != null ? (json['numOfRows'] as int) : 10;
    int totalPage = (totalItemCount / numOfRows).ceil();

    return DrugDoseList(
      json['pageNo'] != null ? (json['pageNo'] as int) : 1,
      (json['totalCount'] != null && json['numOfRows'] != null) ? totalPage : 1,
      json['totalCount'] != null ? (json['totalCount'] as int) : 0,
      json['items'] != null ? (json['items'] as List).map((e) => DrugDoseListItem.fromJson(e)).toList() : [],
    );
  }
}