import 'package:app_drug_daily_dose/model/drug_dose_list_item.dart';
import 'package:flutter/material.dart';

class ComponentDrugList extends StatelessWidget {
  const ComponentDrugList({super.key, required this.item, required this.callback});

  final DrugDoseListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.fromLTRB(20, 5, 20, 5),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
              color: const Color.fromRGBO(50, 50, 50, 100), width: 3
          ),
          borderRadius: BorderRadius.circular(10),
          image: const DecorationImage(
            image: AssetImage('assets/drug.png'),
            opacity: 30,
            alignment: Alignment.bottomRight,
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              '(${item.CPNT_CD})${item.DRUG_CPNT_KOR_NM}',
              style: const TextStyle(
                  fontWeight: FontWeight.w600
              ),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              item.DRUG_CPNT_ENG_NM,
              style: const TextStyle(
                color: Color.fromRGBO(10, 10, 10, 100),
              ),
            ),
            const Divider(),
            Text('제형코드: ${item.FOML_CD.isNotEmpty ? item.FOML_CD : '-'}'),
            Text('제형명: ${item.FOML_NM.isNotEmpty ? item.FOML_NM : '-'}'),
            Text('투여경로: ${item.DOSAGE_ROUTE_CODE.isNotEmpty ? item
                .DOSAGE_ROUTE_CODE : '-'}'),
            Text('투여단위: ${item.DAY_MAX_DOSG_QY_UNIT.isNotEmpty ? item
                .DAY_MAX_DOSG_QY_UNIT : '-'}'),
            Text('1일 최대 투여량: ${item.DAY_MAX_DOSG_QY.isNotEmpty ? item
                .DAY_MAX_DOSG_QY : '-'}'),
          ],
        ),
      ),
    );
  }
}
