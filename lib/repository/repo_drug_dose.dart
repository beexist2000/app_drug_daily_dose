import 'package:app_drug_daily_dose/config/config_api.dart';
import 'package:app_drug_daily_dose/model/drug_dose_list.dart';
import 'package:app_drug_daily_dose/model/drug_dose_response.dart';
import 'package:dio/dio.dart';

class RepoDrugDose {
  final String _baseUrl = '$apiDrugDailyDose/1471000/DayMaxDosgQyByIngdService/getDayMaxDosgQyByIngdInq';

  Future<DrugDoseList> getList({int page = 1, String CPNT_CD = '', String DRUG_CPNT_KOR_NM = '', String DRUG_CPNT_ENG_NM = ''}) async {
    Map<String, dynamic> params = {};
    params['pageNo'] = page;
    params['serviceKey'] = Uri.encodeFull(apiDrugDailyDoseKey);
    params['type'] = 'json';

    if(CPNT_CD.isNotEmpty) params['CPNT_CD'] = Uri.encodeFull(CPNT_CD);
    if(DRUG_CPNT_KOR_NM.isNotEmpty) params['DRUG_CPNT_KOR_NM'] = Uri.encodeFull(DRUG_CPNT_KOR_NM);
    if(DRUG_CPNT_ENG_NM.isNotEmpty) params['DRUG_CPNT_ENG_NM'] = Uri.encodeFull(DRUG_CPNT_ENG_NM);

    Dio dio = Dio();

    final response = await dio.get(
      _baseUrl,
      queryParameters: params,
      options: Options(
        followRedirects: false,
      ),
    );

    DrugDoseResponse resultModel = DrugDoseResponse.fromJson(response.data);
    return resultModel.body!;
  }
}