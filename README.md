# 약 일일 투여량 검색 APP
***
(개인 프로젝트) 약 일일 투여량을 검색할 수 있는 APP / 공공데이터 OPEN API 사용
***

### VERSION
```
0.0.1
```

### LANGUAGE
```
Dart 2.18.1
Flutter 3.3.2
```

### DURATION
```
총 제작일수 : 2일
```

### 기능
```
* 의약품 조회

* 검색
```

### 실제 앱 화면
> * 의약품 조회
>
>![drug_main](./images/drug_main.PNG)

> * 의약품 검색
>
>![drug_search](./images/drug_search.PNG)
