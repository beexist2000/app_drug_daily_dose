import 'package:app_drug_daily_dose/components/component_appbar_popup.dart';
import 'package:app_drug_daily_dose/components/component_search_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageSearch extends StatefulWidget {
  const PageSearch({super.key, required this.CPNT_CD, required this.DRUG_CPNT_KOR_NM, required this.DRUG_CPNT_ENG_NM});

  final String CPNT_CD;
  final String DRUG_CPNT_KOR_NM;
  final String DRUG_CPNT_ENG_NM;

  @override
  State<PageSearch> createState() => _PageSearchState();
}

class _PageSearchState extends State<PageSearch> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '검색화면',
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: SizedBox(
          height: 40,
          child: OutlinedButton(
            onPressed: () {
              String inputCPNT_CD = _formKey.currentState!.fields['CPNT_CD']!.value;
              String inputDRUG_CPNT_KOR_NM = _formKey.currentState!.fields['DRUG_CPNT_KOR_NM']!.value;
              String inputDRUG_CPNT_ENG_NM = _formKey.currentState!.fields['DRUG_CPNT_ENG_NM']!.value;

              Navigator.pop(
                  context,
                  [true, inputCPNT_CD, inputDRUG_CPNT_KOR_NM, inputDRUG_CPNT_ENG_NM]
              );
            },
            child: const Text('검색하기'),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        FormBuilder(
          key: _formKey,
          // enabled: false,
          onChanged: () {
            _formKey.currentState!.save();
          },
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                ComponentSearchTextField(
                  name: 'CPNT_CD',
                  initialValue: widget.CPNT_CD,
                  labelText: '성분코드',
                ),
                const SizedBox(
                  height: 15,
                ),
                ComponentSearchTextField(
                    name: 'DRUG_CPNT_KOR_NM',
                    initialValue: widget.DRUG_CPNT_KOR_NM,
                    labelText: '성분명(한글)'
                ),
                const SizedBox(
                  height: 15,
                ),
                ComponentSearchTextField(
                    name: 'DRUG_CPNT_ENG_NM',
                    initialValue: widget.DRUG_CPNT_ENG_NM,
                    labelText: '성분명(영어)'
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
