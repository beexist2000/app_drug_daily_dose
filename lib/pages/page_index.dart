import 'package:app_drug_daily_dose/components/component_appbar_filter.dart';
import 'package:app_drug_daily_dose/components/component_count_title.dart';
import 'package:app_drug_daily_dose/components/component_custom_loading.dart';
import 'package:app_drug_daily_dose/components/component_drug_list.dart';
import 'package:app_drug_daily_dose/components/component_no_contents.dart';
import 'package:app_drug_daily_dose/model/drug_dose_list_item.dart';
import 'package:app_drug_daily_dose/pages/page_search.dart';
import 'package:app_drug_daily_dose/repository/repo_drug_dose.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> with AutomaticKeepAliveClientMixin {
  final _scrollController = ScrollController();

  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;
  List<DrugDoseListItem> _list = [];

  String _CPNT_CD = '';
  String _DRUG_CPNT_KOR_NM = '';
  String _DRUG_CPNT_ENG_NM = '';

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems();
  }

  Future<void> _loadItems({bool refresh = false}) async {
    if(refresh) {
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
      _list = [];
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(cancelFunc: cancelFunc);
      });

      await RepoDrugDose()
          .getList(
              page: _currentPage,
              CPNT_CD: _CPNT_CD,
              DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM,
              DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM)
          .then((res) {
        BotToast.closeAllLoading();
        setState(() {
          _totalPage = res.totalPage;
          _totalItemCount = res.totalItemCount;
          _list = [..._list, ...res.items];

          _currentPage++;
        });
      }).catchError((err) => BotToast.closeAllLoading());
    }

    if(refresh) {
      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarFilter(
        title: '의약품 리스트',
        icon: Icons.search,
        callback: () async {
          final resultSearch = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>
                PageSearch(
                  CPNT_CD: _CPNT_CD,
                  DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM,
                  DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM,
                ),
            ),
          );

          if (resultSearch != null && resultSearch[0]) {
            _CPNT_CD = resultSearch[1];
            _DRUG_CPNT_KOR_NM = resultSearch[2];
            _DRUG_CPNT_ENG_NM = resultSearch[3];

            _loadItems(refresh: true);
          }
        },
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          ComponentCountTitle(icon: Icons.add, count: _totalItemCount, unitName: '건', itemName: '의약품 정보'),
          _buildBody(),
        ],
      ),
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentDrugList(item: _list[index], callback: () {}),
          ),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 40 - 20,
        child: const ComponentNoContents(icon: Icons.not_interested, msg: '의약품 데이터가 없습니다'),
      );
    }
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
